"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
//Định nghĩa biến toàn cục để lưu gói combo được chọn
//Mỗi khi khách chọn Menu S, M, L bạn lại đổi giá trị properties của nó
var gSelectedMenuStructure = {
    kichCo: "...",
    duongKinh: 0,
    suon: 0,
    salad: 0,
    soLuongNuoc: 0,
    thanhTien: 0,
    };
//Định nghĩa biến toàn cục để lưu loại pizza được chọn, mỗi khi khách chọn, bạn lại đổi giá trị cho nó
var gSelectedPizzaType = {
    loaiPizza: "...", //Ocean Mania, Hawaii, Bacon
    };
//Danh sách các Mã Giảm Giá
var gDiscountVouchers;
//Biến toàn cục Order Id
var gOrderId;
//Biến toàn cục tạo đơn hàng 
var gNewOrderObj = {
    kichCo: "",
    duongKinh: "",
    suon: "",
    salad: "",
    loaiPizza: "",
    idVourcher: "",
    idLoaiNuocUong: "",
    soLuongNuoc: "",
    hoTen: "",
    thanhTien: "",
    email: "",
    soDienThoai: "",
    diaChi: "",
    giamGia: "",
    loiNhan: "",
};
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function(){
    $("#btn-size-small").on("click", function(){
        onBtnSizeSmallClick()
    });
    $("#btn-size-medium").on("click", function(){
        onBtnSizeMediumClick()
    });
    $("#btn-size-large").on("click", function(){
        onBtnSizeLargeClick()
    });
    $("#btn-type-ocean-mania").on("click", function(){
        onBtnTypeOceanManiaClick()
    });
    $("#btn-type-hawaii").on("click", function(){
        onBtnTypeHawaiiClick()
    });
    $("#btn-type-bacon").on("click", function(){
        onBtnTypeBaconClick()
    })
    getDrinkListAjaxClick();
    $("#btn-create-order").on("click", function(){
        onBtnCreateOrderClick();
    });
    $("#btn-confirm").on("click", function(){
        onBtnConfirmOrderClick();
    });  

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//MENU COMBO PIZZA 365
//Hàm thực thi khi size SMALL được chọn
function onBtnSizeSmallClick(){
    //Tạo một đối tượng gói dịch vụ, được tham số hoá
    var vSelectedMenuStructure = getType("Size Small", "20cm", 2, "200g", 2, 150000);
    //Gọi Method hiển thị thông tin
    vSelectedMenuStructure.displayInConsoleLog();
    //Đổi màu Button
    changePlanButtonColor("Size Small");
    //Gán giá trị của plan được chọn vào biến toàn cục để lưu tại đó
    gSelectedMenuStructure.kichCo = vSelectedMenuStructure.kichCo;
    gSelectedMenuStructure.duongKinh = vSelectedMenuStructure.duongKinh;
    gSelectedMenuStructure.suon = vSelectedMenuStructure.suon;
    gSelectedMenuStructure.salad = vSelectedMenuStructure.salad;
    gSelectedMenuStructure.soLuongNuoc = vSelectedMenuStructure.soLuongNuoc;
    gSelectedMenuStructure.thanhTien = vSelectedMenuStructure.thanhTien;
}
//Hàm thực thi khi size MEDIUM được chọn
function onBtnSizeMediumClick(){
    //Tạo một đối tượng gói dịch vụ, được tham số hoá
    var vSelectedMenuStructure = getType("Size Medium", "25cm", 4, "300g", 3, 200000);
    //Gọi method hiển thị thông tin
    vSelectedMenuStructure.displayInConsoleLog();
    //Đổi màu button
    changePlanButtonColor("Size Medium");
    //Gán giá trị của plan được chọn vào biến toàn cục để lưu tại đó
    gSelectedMenuStructure.kichCo = vSelectedMenuStructure.kichCo;
    gSelectedMenuStructure.duongKinh = vSelectedMenuStructure.duongKinh;
    gSelectedMenuStructure.suon = vSelectedMenuStructure.suon;
    gSelectedMenuStructure.salad = vSelectedMenuStructure.salad;
    gSelectedMenuStructure.soLuongNuoc = vSelectedMenuStructure.soLuongNuoc;
    gSelectedMenuStructure.thanhTien = vSelectedMenuStructure.thanhTien;
}
//Hàm thực thi khi size LARGE được chọn
function onBtnSizeLargeClick(){
    //Tạo một đối tượng gói dịch vụ, được tham số hoá 
    var vSelectedMenuStructure = getType("Size Large", "30cm", 8, "500g", 4, 250000);
    //Gọi method hiển thị thông tin
    vSelectedMenuStructure.displayInConsoleLog();
    //Đổi màu button
    changePlanButtonColor("Size Large");
    //Gán giá trị của plan được chọn vào biến toàn cục để lưu tại đó
    gSelectedMenuStructure.kichCo = vSelectedMenuStructure.kichCo;
    gSelectedMenuStructure.duongKinh = vSelectedMenuStructure.duongKinh;
    gSelectedMenuStructure.suon = vSelectedMenuStructure.suon;
    gSelectedMenuStructure.salad = vSelectedMenuStructure.salad;
    gSelectedMenuStructure.soLuongNuoc = vSelectedMenuStructure.soLuongNuoc;
    gSelectedMenuStructure.thanhTien = vSelectedMenuStructure.thanhTien;
}

//MENU OCEAN MANIA - HAWAII - BACON
//Hàm thực thi khi OCEAN MANIA được chọnchọn
function onBtnTypeOceanManiaClick(){
    gSelectedPizzaType.loaiPizza = "Ocean Mania";
    console.log(gSelectedPizzaType);
    //Đổi màu Button
    changeTypeOfPizzaButtonColor("Ocean Mania");
}
//Hàm thực thi khi HAWAII được chọn
function onBtnTypeHawaiiClick(){
    gSelectedPizzaType.loaiPizza = "Hawaii";
    console.log(gSelectedPizzaType);
    //Đổi màu Button
    changeTypeOfPizzaButtonColor("Hawaii");
}
//Hàm thực thi khi BACON được chọn
function onBtnTypeBaconClick(){
    gSelectedPizzaType.loaiPizza = "Bacon";
    console.log(gSelectedPizzaType);
    //Đổi màu Button
    changeTypeOfPizzaButtonColor("Bacon");
}
//Hàm thực thi khi nút 'Gửi' được ấn
function onBtnCreateOrderClick(){
    "use strict";
    //Bước 0: Khai báo đối tượng "Đơn hàng"
    //Bước 1: Thu thập dữ liệu
    readDataUser(gNewOrderObj);
    //Bước 2: Validate - Kiểm tra dữ liệu
    var vDataValidation = validateData(gNewOrderObj);
    //Bước 3: Ghi dữ liệu ra web
    if (vDataValidation == true){
        writeDataUser(gNewOrderObj);
        //Hiển thị modal lên
        $("#insert-order-modal").modal("show");
        handleOrderDetail(gNewOrderObj);
    }
}
//Hàm thực thi khi nút 'Tạo Đơn' được ấn
function onBtnConfirmOrderClick(){
    "use strict";
    //Bước 0: Khai báo đối tượng "Đơn hàng"
    //Bước 1: Thu thập dữ liệu (Không có)
    //Bước 2: Validate dữ liệu (Không có)
    //Bước 3: Ghi dữ liệu ra web
    getCreateOrderAjaxClick(gNewOrderObj);

}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//Function trả lại một đối tượng kích cỡ (gói kích cỡ) được tham số
function getType(paramkichCo, paramduongKinh, paramsuon, paramsalad, paramsoLuongNuoc, paramthanhTien) {
    var vSelectedMenuStructure = {
        kichCo: paramkichCo,
        duongKinh: paramduongKinh,
        suon: paramsuon,
        salad: paramsalad,
        soLuongNuoc: paramsoLuongNuoc,
        thanhTien: paramthanhTien,
        //method display menu infor - phương thức hiện gói Thực Đơn được chọn
        displayInConsoleLog() { //this = "Đối tượng "
            console.log("%MENU SELECTED - Gói thực đơn được chọn...");
            console.log(this.kichCo);
            console.log("Đường kính: " + this.duongKinh);
            console.log("Sườn nướng: " + this.suon);
            console.log("Salad: " + this.salad);
            console.log("Số lượng nước: " + this.soLuongNuoc);
            console.log("Giá: " + this.thanhTien);
        }
    }
    return vSelectedMenuStructure; //trả lại đối tượng có đủ dữ liệu (attribute) và các methods (phương thức)
}
//Khi LOAD danh sách nước uống (Drink list)
function getDrinkListAjaxClick(){
    "use strict";
    $.ajax({
        url: "http://42.115.221.44:8080/devcamp-pizza365/drinks",
        dataType: "json",
        type: "GET",
        success: function (res){
            handleDrinkList(res);
            console.log(res);
        },
        error: function(ajaxContext){
            alert(ajaxContext.responseText);
        }
    });
}
//Hàm tạo option cho Select Đồ Uống
function handleDrinkList(paramDrink){
    "use strict";
    $.each(paramDrink, function(i, item){
        $("#select-drink").append($('<option>',{
            text: item.tenNuocUong,
            value:item.maNuocUong
        }))
    })
}
//Bước 1 - Đọc dữ liệu (khi ấn nút 'Gửi')
function readDataUser(paramNewOrderObj){
    "use strict";
    var vFullNameElement = $.trim($("#inp-fullname").val());
    var vEmailElement = $.trim($("#inp-email").val());
    var vMobileElement = $.trim($("#inp-dien-thoai").val());
    var vAddressElement = $.trim($("#inp-dia-chi").val());
    var vVoucherIdElement = $.trim($("#inp-voucher-id").val());
    var vMessageElement = $.trim($("#inp-message").val());
    var vSelectDrinkElement = $("#select-drink").val();
    
    paramNewOrderObj.hoTen = vFullNameElement;
    paramNewOrderObj.email = vEmailElement;
    paramNewOrderObj.soDienThoai = vMobileElement;
    paramNewOrderObj.diaChi = vAddressElement;
    paramNewOrderObj.idVourcher = vVoucherIdElement;
    paramNewOrderObj.loiNhan = vMessageElement;
    paramNewOrderObj.idLoaiNuocUong = vSelectDrinkElement;
    
    paramNewOrderObj.kichCo = gSelectedMenuStructure.kichCo;
    paramNewOrderObj.duongKinh = gSelectedMenuStructure.duongKinh;
    paramNewOrderObj.suon = gSelectedMenuStructure.suon;
    paramNewOrderObj.salad = gSelectedMenuStructure.salad;
    paramNewOrderObj.soLuongNuoc = gSelectedMenuStructure.soLuongNuoc;
    paramNewOrderObj.thanhTien = gSelectedMenuStructure.thanhTien;
    paramNewOrderObj.loaiPizza = gSelectedPizzaType.loaiPizza;

}
//Bước 2 - Kiểm tra dữ liệu (khi ấn nút 'Gửi')
function validateData(paramNewOrderObj){
    "use strict";
    if (gSelectedMenuStructure.kichCo === "..."){
        alert("40 - Bạn chưa chọn kích cỡ");
        return false;
    }
    if (gSelectedPizzaType.loaiPizza === "..."){
        alert("45 - Bạn chưa chọn loại pizza");
        return false;
    }
    if (paramNewOrderObj.idLoaiNuocUong === "Not_Selected_Drink") {
        alert("50 - Bạn chưa chọn nước uống");
        return false;
    }
    if (paramNewOrderObj.hoTen === "") {
        alert("100 - Bạn chưa điền tên");
        return false;
    }
    if (paramNewOrderObj.email === "") {
        alert("200 - Bạn chưa điền email");
        return false;
    }
    if (paramNewOrderObj.email.includes("@") === false) {
        alert("201 - Email không có @");
        return false;
    }
    if (paramNewOrderObj.soDienThoai === "") {
        alert("300 - Bạn chưa điền số điện thoại");
        return false;
    }
    if (isNaN (paramNewOrderObj.soDienThoai)) {
        alert("301 - Dữ liệu điện thoại là số");
        return false;
    }
    if (paramNewOrderObj.diaChi === "") {
        alert("400 - Bạn chưa điền địa chỉ");
        return false;
    }
    if (paramNewOrderObj.idVourcher === "") {
        alert("100 - Bạn chưa điền mã giảm giá");
        return false;
    }
    if (paramNewOrderObj.loiNhan === "") {
        alert("500 - Bạn chưa điền lời nhắn");
        return false;
    }
    return true;
}
//Bước 3 - Hiển thị dữ liệu (khi ấn nút 'Gửi')
function writeDataUser(paramNewOrderObj){
    "use strict";
    getDiscount (paramNewOrderObj.idVourcher);
    console.log(gDiscountVouchers);
    var vPrice = gSelectedMenuStructure.thanhTien - (gSelectedMenuStructure.thanhTien * gDiscountVouchers) / 100;
    console.log("ĐƠN HÀNG ĐƯỢC XÁC NHẬN!")
    console.log("Thông tin khách hàng là: ");
    console.log("Họ và tên: " + paramNewOrderObj.hoTen);
    console.log("Email: " + paramNewOrderObj.email);
    console.log("Điện thoại: " + paramNewOrderObj.soDienThoai);
    console.log("Địa chỉ: " + paramNewOrderObj.diaChi);
    console.log("Lời nhắn: " + paramNewOrderObj.loiNhan);
    console.log("------------");
    console.log("Thông tin order đã xác nhận là: ");
    console.log("Kích cỡ: " + gSelectedMenuStructure.kichCo);
    console.log("Đường kính: " + gSelectedMenuStructure.duongKinh);
    console.log("Sườn: " + gSelectedMenuStructure.suon);
    console.log("Salad: " + gSelectedMenuStructure.salad);
    console.log("Nước uống: " + paramNewOrderObj.idLoaiNuocUong);
    console.log("Số lượng nước: " + gSelectedMenuStructure.soLuongNuoc);
    console.log("------------");
    console.log("Thông tin tổng tiền đã xác nhận: ");
    console.log("Mã Voucher: " + paramNewOrderObj.idVourcher);
    console.log("Thành tiền: " + gSelectedMenuStructure.thanhTien);
    console.log("Discount: " + gDiscountVouchers);
    console.log("Phải thanh toán: " + vPrice);
}

//Hàm gọi API xử lý Mã Giảm Giá
function getDiscount(paramVoucherId){
    //Bước 1: Read Data 
    "use strict";
    //Bước 2: Kiểm tra dữ liệu input (không có)
    //Bước 3: Gọi Api bằng Ajax
    $.ajax({
        url: "http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/" + paramVoucherId,
        type: 'GET',
        dataType: 'json',
        async: false,
        success: function (res){
            console.log(res);
            gDiscountVouchers = res.phanTramGiamGia;
        },
        error: function(ajaxContext){
            alert(ajaxContext.responseText);
            console.log("Không tìm thấy voucher");
            gDiscountVouchers = 0;
        }
    });
}
/*
//Hàm gọi API xử lý Order Id
function getOrderIdAjaxClick(paramOrderId){
    "use strict";
    //Bước 1: Read Data (không có)
    //Bước 2: Kiểm tra dữ liệu input (không có)
    //Bước 3: gọi API bằng AJAX
    $.ajax({
        url: 'http://42.115.221.44:8080/devcamp-pizza365/orders' + paramOrderId,
        dataType: 'jason',
        type: 'GET',
        async: false,
        success: function (res){
            console.log(res);
            //handleOrderIdDetail(res);
        },
        error: function(ajaxContext){
            alert(ajaxContext.responseText);
        }
    });
}
*/
//Hàm gọi API tạo đơn hàng mới
function getCreateOrderAjaxClick(paramNewOrderObj){
    debugger;
    "use strict";
    //Bước 1: Read Data (không có)
    //Bước 2: Kiểm tra dữ liệu (không có)
    //Bước 3: Gọi API bằng AJAX
    $.ajax({
        url: 'http://42.115.221.44:8080/devcamp-pizza365/orders',
        type: 'POST',
        data: JSON.stringify(paramNewOrderObj),
        contentType: 'application/json;charset=UTF-8',
        async: false,
        success: function (res){
            console.log(res);
            //Ẩn modal Thông Tin Đơn Hàng
            $("#insert-order-modal").modal("hide");
            //Hiển thị modal OrderId lên
            $("#insert-confirm-modal").modal("show");
            gOrderId = res.orderId;
            $("#input-orderid").val(gOrderId);
        },
        error: function (error){
            alert(error.responseText);
        }
    }); 
}
//Hàm đổ Data vào Modal
function handleOrderDetail(paramNewOrderObj){
    "use strict";
    var vPrice = gSelectedMenuStructure.thanhTien - (gSelectedMenuStructure.thanhTien * gDiscountVouchers) / 100;
    $("#input-fullname").val(paramNewOrderObj.hoTen);
    $("#input-mobile").val(paramNewOrderObj.soDienThoai);
    $("#input-address").val(paramNewOrderObj.diaChi);
    $("#input-message").val(paramNewOrderObj.loiNhan);
    $("#input-voucherid").val(paramNewOrderObj.idVourcher);
    $("#input-order-detail").val(
        "Xác nhận: Khách hàng " + paramNewOrderObj.hoTen + ", Số điện thoại: " + paramNewOrderObj.soDienThoai + ", Địa chỉ: " + paramNewOrderObj.diaChi + ", " +
        "Menu " + gSelectedMenuStructure.kichCo + ", sườn nướng " + gSelectedMenuStructure.suon + ", nước " + gSelectedMenuStructure.soLuongNuoc + ", salad " + gSelectedMenuStructure.salad + ", " +
        "Loại pizza: " + gSelectedPizzaType.loaiPizza + "," + ", Giá: " + gSelectedMenuStructure.thanhTien + ", Mã giảm giá: " + paramNewOrderObj.idVourcher + ", " +
        "Phải thanh toán: " + vPrice + ", Giá giảm: " + gDiscountVouchers + "%");
    }
/*
//Hàm xoá trắng modal form Create Order
function resetCreateOrderForm(){
    $("#input-fullname").val("");
    $("#input-mobile").val("");
    $("#input-address").val("");
    $("#input-message").val("");
    $("#input-voucherid").val("");
    $("#input-order-detail").val("");
}
*/
//Đổi màu button ('Chọn') để phân biệt gói kích cỡ pizza được chọn
changePlanButtonColor(gSelectedMenuStructure.kichCo);
//Hàm xử lý sự kiện nut btn color
  function changePlanButtonColor(paramMenuName) {
    "use strict";
    var vBtnSmall = $("#btn-size-small");
    var vBtnMedium = $("#btn-size-medium");
    var vBtnLarge = $("#btn-size-large");
        //Set btn menu về màu mặc định (Cam)
        if(paramMenuName === "...") {
            vBtnSmall.removeClass().addClass("btn w-100 bg-color-Orange");
            vBtnMedium.removeClass().addClass("btn w-100 bg-color-Orange");
            vBtnLarge.removeClass().addClass("btn w-100 bg-color-Orange");
        } else if (paramMenuName === "Size Small") { //Khi chọn size S
            vBtnSmall.removeClass().addClass("btn w-100 bg-color-green");
            vBtnMedium.removeClass().addClass("btn w-100 bg-color-Orange");
            vBtnLarge.removeClass().addClass("btn w-100 bg-color-Orange");
        } else if (paramMenuName === "Size Medium") { //Khi chọn size M
            vBtnSmall.removeClass().addClass("btn w-100 bg-color-Orange");
            vBtnMedium.removeClass().addClass("btn w-100 bg-color-green");
            vBtnLarge.removeClass().addClass("btn w-100 bg-color-Orange");
        } else if (paramMenuName === "Size Large") { //Khi chọn size L
            vBtnSmall.removeClass().addClass("btn w-100 bg-color-Orange");
            vBtnMedium.removeClass().addClass("btn w-100 bg-color-Orange");
            vBtnLarge.removeClass().addClass("btn w-100 bg-color-green");
        }
    }
//Đổi màu button ('Chọn') để phân biệt loại pizza được chọn
changeTypeOfPizzaButtonColor(gSelectedPizzaType.loaiPizza);
//Hàm xử lý sự kiện nút đổi màu - phân biệt loại pizza 
function changeTypeOfPizzaButtonColor(paramPizzaType){
    "use strict";
    var vBtnOceanMania = $("#btn-type-ocean-mania");
    var vBtnHawaii = $("#btn-type-hawaii");
    var vBtnBacon = $("#btn-type-bacon");
        //Set btn menu về màu mặc định (Cam)
        if(paramPizzaType === ""){
            vBtnOceanMania.removeClass().addClass("btn w-100 bg-color-Orange");
            vBtnHawaii.removeClass().addClass("btn w-100 bg-color-Orange");
            vBtnBacon.removeClass().addClass("btn w-100 bg-color-Orange");
        } else if (paramPizzaType === "Ocean Mania"){ //Khi chọn OCEAN MANIA
            vBtnOceanMania.removeClass().addClass("btn w-100 bg-color-green");
            vBtnHawaii.removeClass().addClass("btn w-100 bg-color-Orange");
            vBtnBacon.removeClass().addClass("btn w-100 bg-color-Orange");
        } else if (paramPizzaType === "Hawaii"){ //Khi chọn HAWAII
            vBtnOceanMania.removeClass().addClass("btn w-100 bg-color-Orange");
            vBtnHawaii.removeClass().addClass("btn w-100 bg-color-green");
            vBtnBacon.removeClass().addClass("btn w-100 bg-color-Orange");
        } else if (paramPizzaType === "Bacon"){ //Khi chọn BACON
            vBtnOceanMania.removeClass().addClass("btn w-100 bg-color-Orange");
            vBtnHawaii.removeClass().addClass("btn w-100 bg-color-Orange");
            vBtnBacon.removeClass().addClass("btn w-100 bg-color-green");
        }
    }
});